<?php
/**
 * K8 Updater
 *
 * @package   K8_Updater
 * @author    Charl Pretorius
 * @license   GPL-2.0+
 * @link      
 */

/*
Plugin Name:       		K8 Plugin Updater MU Version
Plugin URI:        
Description:       		A plugin to automatically update K8 GitHub or Bitbucket hosted plugins and themes into WordPress.
Version:           		1.1.0
Author:            		Charl Pretorius
License:           		GNU General Public License v2
License URI:       		http://www.gnu.org/licenses/gpl-2.0.html
K8 Bitbucket Plugin URI: 	https://bitbucket.org/kri8it/k8-updater
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class K8_PluginUpdates{
	
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $K8_Slug    The ID of this plugin.
	 */
	private $K8_Slug;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	
	/**
	 * Array of K8 Plugins.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $k8_plugins    Array of K8 Plugins.
	 */
	private $k8_plugins;
	
	/**
	 * Array of K8 Themes.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $k8_themes    Array of K8 Themes.
	 */
	private $k8_themes;
	
	/**
	 * Array of K8 Headers.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $k8_extra_theme_headers    Array of K8 Headers.
	 */
	public $k8_extra_theme_headers;
	
	/**
	 * Array of K8 Headers.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $k8_extra_plugin_headers    Array of K8 Headers.
	 */
	public $k8_extra_plugin_headers;
	
	/*
	*  __construct
	*
	*  Initialize filters, action, variables and includes
	*
	*  @type	function
	*  @date	28/11/14
	*  @since	1.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	
	function __construct() {
		
		global $current_user;
						
		if ( true ){
				
			$this->K8_Slug = 'k8_plugin_updates_';
			
			// This MUST come before we get details about the plugins so the headers are correctly retrieved
			$this->add_headers();
			
			$this->k8_plugins = $this->get_plugins();
			$this->k8_themes = $this->get_themes();
			
			if( !empty( $this->k8_plugins ) ){
							
				// override requests for plugin information
		        add_filter( 'plugins_api', array( $this, 'inject_plugin_info' ), 25, 3 ); 
				
				// override requests for theme information
				add_filter( 'themes_api', array( $this, 'inject_theme_info' ), 99, 3 );       
		        
				// insert our update info into the update array maintained by WP
				add_filter( 'site_transient_update_plugins', array( $this, 'inject_update' ) );
				
				// insert our theme update info into the update array maintained by WP
				add_filter( 'pre_set_site_transient_update_themes', array( $this, 'inject_update_themes' ), 19999 );
				
				// rename the zip folder to be the same as the existing repository folder.
				add_filter( 'upgrader_source_selection', array( $this, 'upgrader_source_selection' ), 10, 3 );
			
			}
		
		}
		
	}
	
	/**
	 * Add extra headers via filter hooks
	 */
	public function add_headers() {
		add_filter( 'extra_plugin_headers', array( $this, 'add_plugin_headers' ) );
		add_filter( 'extra_theme_headers', array( $this, 'add_theme_headers' ) );		
	}

	/**
	 * Add extra header to get_plugins();
	 *
	 * @param $extra_headers
	 * @return array
	 */
	public function add_plugin_headers( $extra_headers ) {
			
		$this->k8_extra_plugin_headers     = array(
			'K8 Bitbucket Plugin URI' => 'K8 Bitbucket Plugin URI',
			'K8 Bitbucket Branch'     => 'K8 Bitbucket Branch',
			'Requires WP'          => 'Requires WP',
			'Requires PHP'         => 'Requires PHP',
		);
		
		$extra_headers = array_merge( (array) $extra_headers, (array) $this->k8_extra_plugin_headers );

		return $extra_headers;
		
	}

	/**
	 * Add extra header to get_themes();
	 *
	 * @param $extra_headers
	 * @return array
	 */
	public function add_theme_headers( $extra_headers ) {
			
		$this->k8_extra_theme_headers     = array(
			'K8 Bitbucket Theme URI' => 'K8 Bitbucket Theme URI',
			'K8 Bitbucket Branch'    => 'K8 Bitbucket Branch',
			'Requires WP'         => 'Requires WP',
			'Requires PHP'        => 'Requires PHP',
		);
		
		$extra_headers = array_merge( (array) $extra_headers, (array) $this->add_theme_headers );

		return $extra_headers;
		
	}
	
	/**
	 * Rename the zip folder to be the same as the existing repository folder.
	 *
	 * Github delivers zip files as <Repo>-<Branch>.zip
	 *
	 * @global WP_Filesystem $wp_filesystem
	 *
	 * @param string $source
	 * @param string $remote_source Optional.
	 * @param object $upgrader      Optional.
	 *
	 * @return string
	 */
	public function upgrader_source_selection( $source, $remote_source , $upgrader ) {

		global $wp_filesystem;

		if ( isset( $source ) ) {
			foreach ( (array) $this->k8_plugins as $k8_repo ) {
				if ( stristr( basename( $source ), $k8_repo->repo ) ) {
					$repo = $k8_repo->repo;
				}
			}
		}

		// Check for upgrade process, return if both are false
		if ( ! is_a( $upgrader, 'Plugin_Upgrader' ) && ! is_a( $upgrader, 'Theme_Upgrader' ) ) {
			return $source;
		}

		// If the values aren't set, or it's not GitHub-sourced, abort
		if ( ! isset( $source, $remote_source, $repo ) || false === stristr( basename( $source ), $repo ) ) {
			return $source;
		}

		$corrected_source = trailingslashit( $remote_source ) . trailingslashit( $repo );
		$upgrader->skin->feedback(
			sprintf(
				__( 'Renaming %s to %s&#8230;', 'k8' ),
				'<span class="code">' . basename( $source ) . '</span>',
				'<span class="code">' . basename( $corrected_source ) . '</span>'
			)
		);

		// If we can rename, do so and return the new name
		if ( $wp_filesystem->move( $source, $corrected_source, true ) ) {
			$upgrader->skin->feedback( __( 'Rename successful&#8230;', 'k8' ) );
			
			wp_clean_plugins_cache();
			
			// only allow transient to be deleted once per page load
			delete_transient( 'k8_get_remote_plugins_info' );
			delete_transient( 'k8_get_remote_themes_info' );
			
			return $corrected_source;
		}

		// Otherwise, return an error
		$upgrader->skin->feedback( __( 'Unable to rename downloaded repository.', 'k8' ) );
		return new WP_Error();
	}

	/**
	* Get array of all themes in multisite
	*
	* wp_get_themes does not seem to work under network activation in the same way as in a single install.
	* http://core.trac.wordpress.org/changeset/20152
	*
	* @return array
	*/
	protected function multisite_get_themes() {
		$themes     = array();
		$theme_dirs = scandir( get_theme_root() );
		$theme_dirs = array_diff( $theme_dirs, array( '.', '..', '.DS_Store', 'index.php' ) );

		foreach ( (array) $theme_dirs as $theme_dir ) {
			$themes[] = wp_get_theme( $theme_dir );
		}

		return $themes;
	}
		
	/**
	*  get_plugins
	*
	*  Get details of K8-sourced plugins from those that are installed.
	*
	*  @type	function
	*  @date	28/11/14
	*  @since	1.0.0
	*    
	*  @return array Indexed array of associative arrays of plugin details.
	*/
	
	protected function get_plugins() {
		
		// Ensure get_plugins() function is available.
		include_once( ABSPATH . '/wp-admin/includes/plugin.php' );

		$plugins     = get_plugins();
		$k8_plugins = array();

		foreach ( (array) $plugins as $plugin => $headers ) {
				
			if ( empty( $headers['K8 Bitbucket Plugin URI'] ) ) {
				continue;
			}
			
			$k8_repo = array();
			
			/* Set Defaults */
			$k8_repo['type']        		  = 'plugin';			
			$k8_repo['remote_version']        = '0.0.0';
			$k8_repo['newest_tag']            = '0.0.0';
			$k8_repo['download_link']         = null;
			$k8_repo['tags']                  = array();
			$k8_repo['rollback']              = array();
			$k8_repo['k8_repo']->sections['changelog'] = 'No changelog is available via Updater. Create a file <code>CHANGES.md</code> or <code>CHANGELOG.md</code> in your repository.';
			$k8_repo['requires']              = null;
			$k8_repo['tested']                = null;
			$k8_repo['downloaded']            = 0;
			$k8_repo['last_updated']          = null;
			$k8_repo['rating']                = 0;
			$k8_repo['num_ratings']           = 0;
			$k8_repo['transient']             = array();
			$k8_repo['repo_meta']             = array();
			$k8_repo['watchers']              = 0;
			$k8_repo['forks']                 = 0;
			$k8_repo['open_issues']           = 0;
			$k8_repo['score']                 = 0;
			$k8_repo['requires_wp_version']   = '0.0.0';
			$k8_repo['requires_php_version']  = '5.2.3';
												
			$k8_repo = array_merge( $this->get_local_plugin_meta( $headers ), $k8_repo );
			
			$k8_repo['slug']                    = $plugin;
			
			$plugin_data                        = get_plugin_data( WP_PLUGIN_DIR . '/' . $k8_repo['slug'] );
			
			$k8_repo['author']                  = $plugin_data['AuthorName'];
			$k8_repo['name']                    = $plugin_data['Name'];
			$k8_repo['local_version']           = strtolower( $plugin_data['Version'] );
			$k8_repo['sections']['description'] = $plugin_data['Description'];
			
			$k8_plugins[ $k8_repo['repo'] ]     = (object) $k8_repo;
			
		}

		return $k8_plugins;
		
	}


	/**
	*  get_themes
	*
	*  Get details of K8-sourced themes from those that are installed.
	*
	*  @type	function
	*  @date	28/11/14
	*  @since	1.0.0
	*    
	*  @return array Indexed array of associative arrays of theme details.
	*/
	
	protected function get_themes() {
				
		$themes = array();
		
		if( is_multisite() ):
			$themes	= $this->multisite_get_themes();
		else:
			$themes = wp_get_themes();
		endif;
		
		$k8_themes = array();

		foreach ( (array) $themes as $theme => $headers ) {
				
			if ( empty( $headers['K8 Bitbucket Theme URI'] ) ) {
				continue;
			}
			
			$k8_repo = array();
			
			/* Set Defaults */
			$k8_repo['type']        		  = 'theme';
			$k8_repo['remote_version']        = '0.0.0';
			$k8_repo['newest_tag']            = '0.0.0';
			$k8_repo['download_link']         = null;
			$k8_repo['tags']                  = array();
			$k8_repo['rollback']              = array();
			$k8_repo['k8_repo']->sections['changelog'] = 'No changelog is available via Updater. Create a file <code>CHANGES.md</code> or <code>CHANGELOG.md</code> in your repository.';
			$k8_repo['requires']              = null;
			$k8_repo['tested']                = null;
			$k8_repo['downloaded']            = 0;
			$k8_repo['last_updated']          = null;
			$k8_repo['rating']                = 0;
			$k8_repo['num_ratings']           = 0;
			$k8_repo['transient']             = array();
			$k8_repo['repo_meta']             = array();
			$k8_repo['watchers']              = 0;
			$k8_repo['forks']                 = 0;
			$k8_repo['open_issues']           = 0;
			$k8_repo['score']                 = 0;
			$k8_repo['requires_wp_version']   = '0.0.0';
			$k8_repo['requires_php_version']  = '5.2.3';
			
			$k8_repo = array_merge( $this->get_local_plugin_meta( $headers ), $k8_repo );
			
			$k8_repo['slug']                    = $theme;
			$k8_repo['theme_uri']               = $theme->get( 'ThemeURI' );
			$k8_repo['author']                  = $theme->get( 'Author' );
			$k8_repo['name']                    = $theme->get( 'Name' );
			$k8_repo['local_version']           = strtolower( $theme->get( 'Version' ) );
			$k8_repo['sections']['description'] = $theme->get( 'Description' );
			
			$k8_themes[ $k8_repo['repo'] ]     = (object) $k8_repo;
			
		}

		return $k8_plugins;
		
	}
	
						
	
	/*
	*  inject_plugin_info
	*
	*  description
	*
	*  @type	function
	*  @date	28/11/14
	*  @since	1.0.0
	*
	*  @param	$post_id (int)
	*  @return	$post_id (int)
	*/
	
	function inject_plugin_info( $res, $action = null, $args = null ) {
		
		if ( ! ( 'plugin_information' === $action ) ) {
			return $res;
		}
							
		// vars
        $plugins = $this->k8_get_remote_plugins_info();
        
		foreach ( $plugins as $plugin ) {
			
			if( isset($args->slug) && $args->slug == $plugin->repo ){
				
				$obj = new stdClass();
				
				$obj->slug          = $plugin->repo;
				$obj->plugin_name   = $plugin->name;
				$obj->name          = $plugin->name;
				$obj->author        = $plugin->author;
				$obj->homepage      = $plugin->uri;
				$obj->version       = $plugin->remote_version;
				$obj->sections      = $plugin->sections;
				$obj->requires      = $plugin->requires;
				$obj->tested        = $plugin->tested;
				$obj->downloaded    = $plugin->downloaded;
				$obj->last_updated  = $plugin->last_updated;
				$obj->rating        = $plugin->rating;
				$obj->num_ratings   = $plugin->num_ratings;
				$obj->download_link = $plugin->download_link;				
				
				return $obj;
				
			}
			
		}
		    	
    	// return        
        return $res;
        
	}


	/*
	*  inject_theme_info
	*
	*  description
	*
	*  @type	function
	*  @date	28/11/14
	*  @since	1.0.0
	*
	*  @param	$post_id (int)
	*  @return	$post_id (int)
	*/
	
	function inject_theme_info( $res, $action = null, $args = null ) {
		
		if ( ! ( 'theme_information' === $action ) ) {
			return $res;
		}

		// Early return $false for adding themes from repo
		if ( isset( $args->fields ) && ! $args->fields['sections'] ) {
			return $res;
		}
							
		// vars
        $themes = $this->k8_get_remote_themes_info();
        
		foreach ( $themes as $theme ) {
			
			if( isset($args->slug) && $args->slug == $plugin->repo ){
				
				$obj = new stdClass();
				
				$obj->slug          = $theme->repo;
				$obj->name          = $theme->name;
				$obj->author        = $theme->author;
				$obj->homepage      = $theme->uri;
				$obj->version       = $theme->remote_version;
				$obj->sections      = $theme->sections;
				$obj->description   = implode( "\n", $theme->sections );
				$obj->preview_url   = $theme->theme_uri;
				$obj->requires      = $theme->requires;
				$obj->tested        = $theme->tested;
				$obj->downloaded    = $theme->downloaded;
				$obj->last_updated  = $theme->last_updated;
				$obj->rating        = $theme->rating;
				$obj->num_ratings   = $theme->num_ratings;
				$obj->download_link = $theme->download_link;				
				
				return $obj;
				
			}
			
		}
		
		add_action( 'admin_head', array( $this, 'fix_display_none_in_themes_api' ) );				
		    	
    	// return        
        return $res;
        
	}

	/**
	 * Fix for new issue in 3.9 :-(
	 */
	public function fix_display_none_in_themes_api() {
		echo '<style> #theme-installer div.install-theme-info { display: block !important; }  </style>';
	}	
	
	/*
	*  inject_update
	*
	*  description
	*
	*  @type	function
	*  @date	28/11/14
	*  @since	1.0.0
	*
	*  @param	$post_id (int)
	*  @return	$post_id (int)
	*/
	
	function inject_update( $transient ) {
		
		if ( empty( $transient->checked ) ) {
			return $transient;
		}
						
		// bail early if not admin
		if( !is_admin() ) {
			
			return $transient;
			
		}
				 
        // vars
        $plugins = $this->k8_get_remote_plugins_info();
        
		foreach ( $plugins as $plugin ) {
			
			if( $this->k8_is_update_available( $plugin ) ){
			
				// create new object for updateß
		        $obj = new stdClass();
		        $obj->slug = dirname( $plugin->slug );
		        $obj->new_version = $plugin->remote_version;
		        $obj->url = $plugin->uri;
		        $obj->package = $plugin->download_link;
				
				// add to transient
	        	$transient->response[ $plugin->slug ] = $obj;
			
			}
			
		}		    
		
		// return 
        return $transient;
	}
	
	/*
	*  inject_update_themes
	*
	*  description
	*
	*  @type	function
	*  @date	28/11/14
	*  @since	1.0.0
	*
	*  @param	$post_id (int)
	*  @return	$post_id (int)
	*/
	
	function inject_update_themes( $updates ) {
			
		
		if ( empty( $transient->checked ) ) {
			return $transient;
		}
						
		// bail early if not admin
		if( !is_admin() ) {
			
			return $transient;
			
		}
		
		// vars
        $themes = $this->k8_get_remote_themes_info();
        
		foreach ( $themes as $theme ) {
			
			if( $this->k8_is_update_available( $theme ) ){
			
				// create new object for updateß
		        $obj = new stdClass();
		        $obj->slug = dirname( $theme->slug );
		        $obj->new_version = $theme->remote_version;
		        $obj->url = $theme->uri;
		        $obj->package = $theme->download_link;
				
				// add to transient
	        	$transient->response[ $theme->slug ] = $obj;
			
			}
			
		}		    
		
		// return 
        return $transient;		
		
	}
	
	
	/*
	*  k8_is_update_available
	*
	*  This function will return true if an update is available
	*
	*  @type	function
	*  @date	28/11/2014
	*  @since	1.0.0
	*
	*  @param	n/a
	*  @return	(boolean)
	*/
	
	function k8_is_update_available( $plugin ) {
		
		$update_found = false;
											
		$remote_version = $plugin->remote_version;
		$local_version = $plugin->local_version;			
		
		//echo $plugin->name . ' =================================================================<br/>';
		//echo 'Local Version - ' . $local_version . ' === Remote Version - ' . $remote_version . '<br/>'; 
											
		// return false if no info
		if( empty( $remote_version ) ) {
			
			return false;
			
		}
		
	    
	    // return true if the external version is '>' the current version
		if( version_compare($remote_version, $local_version, '>') ) {
			
	    	$update_found = true;
	    
	    }
				
		//echo '<br/>-------------------------------------------------------------<br/><br/>';		
							
		// return
		return $update_found;
		
	}
	
	
	/*
	*  k8_get_remote_info
	*
	*  This function will return remote plugin data
	*
	*  @type	function
	*  @date	28/11/2014
	*  @since	1.0.0
	*
	*  @param	n/a
	*  @return	(mixed)
	*/
	
	function k8_get_remote_plugins_info() {
		
		// clear transient if force check is enabled
		if( !empty($_GET['force-check']) ) {
			
			// only allow transient to be deleted once per page load
			if( empty($_GET['k8-ignore-force-check']) ) {
				
				delete_transient( 'k8_get_remote_plugins_info' );
				
			}
						
			// update $_GET
			$_GET['k8-ignore-force-check'] = true;
			
		}
		
		
		// get transient
		$transient = get_transient( 'k8_get_remote_plugins_info' );
		
		
		
		if( $transient !== false ) {
														
			return $transient;
		
		}
	
		// vars
		
		$plugins = $this->k8_plugins;
		
		foreach( $plugins as $plugin ){
			
			$remote = new K8_Updater_BitBucket_API( $plugin, $this->k8_extra_plugin_headers );
			
			if ( $remote->get_remote_info( basename( $plugin->slug ) ) ) {
				$remote->get_repo_meta();
				$remote->get_remote_tag();
				$changelog = $this->get_changelog_filename( $plugin );
				if ( $changelog ) {
					$remote->get_remote_changes( $changelog );
				}
				$plugin->download_link = $remote->construct_download_link();
			}
			
		}
				
		$timeout = 12 * HOUR_IN_SECONDS;
		
		
	    // decode
	    if( empty($plugins) ) {
		    
		    $info = 0; // allow transient to be returned, but empty to validate
		    $timeout = 2 * HOUR_IN_SECONDS;
		    
	    }
	    	        
		// update transient
		set_transient('k8_get_remote_plugins_info', $plugins, $timeout );
								
		// return
		return $plugins;
	}

	
	
	/*
	*  k8_get_remote_themes_info
	*
	*  This function will return remote plugin data
	*
	*  @type	function
	*  @date	28/11/2014
	*  @since	1.0.0
	*
	*  @param	n/a
	*  @return	(mixed)
	*/
	
	function k8_get_remote_themes_info() {
		
		// clear transient if force check is enabled
		if( !empty($_GET['force-check']) ) {
			
			// only allow transient to be deleted once per page load
			if( empty($_GET['k8-ignore-force-check']) ) {
				
				delete_transient( 'k8_get_remote_themes_info' );
				
			}
						
			// update $_GET
			$_GET['k8-ignore-force-check'] = true;
			
		}
		
		
		// get transient
		$transient = get_transient( 'k8_get_remote_themes_info' );
		
		
		
		if( $transient !== false ) {
														
			return $transient;
		
		}
	
		// vars
		
		$themes = $this->k8_themes;
		
		foreach( $themes as $theme ){
			
			$remote = new K8_Updater_BitBucket_API( $theme, $this->k8_extra_theme_headers );
			
			if ( $remote->get_remote_info( basename( $theme->slug ) ) ) {
				$remote->get_repo_meta();
				$remote->get_remote_tag();
				$changelog = $this->get_changelog_filename( $theme );
				if ( $changelog ) {
					$remote->get_remote_changes( $changelog );
				}
				$theme->download_link = $remote->construct_download_link();
			}
			
		}
				
		$timeout = 12 * HOUR_IN_SECONDS;
		
		
	    // decode
	    if( empty($theme) ) {
		    
		    $info = 0; // allow transient to be returned, but empty to validate
		    $timeout = 2 * HOUR_IN_SECONDS;
		    
	    }
	    	        
		// update transient
		set_transient('k8_get_remote_themes_info', $theme, $timeout );
								
		// return
		return $theme;
	}
	
			
	
	/**
	* Parse extra headers to determine repo type and populate info
	*
	* @param array of extra headers
	* @return array of repo information
	*
	* parse_url( ..., PHP_URL_PATH ) is either clever enough to handle the short url format
	* (in addition to the long url format), or it's coincidentally returning all of the short
	* URL string, which is what we want anyway.
	*
	*/
	protected function get_local_plugin_meta( $headers ) {
			
		$k8_repo = array();
		
		// Reverse sort to run plugin/theme URI first
		arsort( $this->k8_extra_plugin_headers );
		arsort( $this->k8_extra_theme_headers );
		
		$extra_headers = array_merge( $this->k8_extra_plugin_headers, $this->k8_extra_theme_headers );
		
		foreach ( (array) $extra_headers as $key => $value ) {
			
			switch( $value ) {
				case 'K8 Bitbucket Plugin URI':
					if ( empty( $headers['K8 Bitbucket Plugin URI'] ) ) {
						break;
					}
					$k8_repo['type']       = 'bitbucket_plugin';

					$k8_repo['user']       = parse_url( $headers['K8 Bitbucket Plugin URI'], PHP_URL_USER );
					$k8_repo['pass']       = parse_url( $headers['K8 Bitbucket Plugin URI'], PHP_URL_PASS );
					$owner_repo            = parse_url( $headers['K8 Bitbucket Plugin URI'], PHP_URL_PATH );
					$owner_repo            = trim( $owner_repo, '/' );  // strip surrounding slashes
					$k8_repo['uri']        = 'https://bitbucket.org/' . $owner_repo;
					$owner_repo            = explode( '/', $owner_repo );
					$k8_repo['owner']      = $owner_repo[0];
					$k8_repo['repo']       = $owner_repo[1];
					$k8_repo['local_path'] = WP_PLUGIN_DIR . '/' . $k8_repo['repo'] .'/';
					
					break;
				case 'Bitbucket Branch':
					if ( empty( $headers['Bitbucket Branch'] ) ) {
						break;
					}
					$k8_repo['branch']     = $headers['Bitbucket Branch'];
					break;
				
				case 'K8 Bitbucket Theme URI':
					if ( empty( $headers['K8 Bitbucket Theme URI'] ) ) {
						break;
					}

					$k8_repo['type']		= 'bitbucket_theme';

					$k8_repo['user']      	= parse_url( $headers['K8 Bitbucket Theme URI'], PHP_URL_USER );
					$k8_repo['pass']      	= parse_url( $headers['K8 Bitbucket Theme URI'], PHP_URL_PASS );
					$owner_repo             = parse_url( $headers['K8 Bitbucket Theme URI'], PHP_URL_PATH );
					$owner_repo             = trim( $owner_repo, '/' );
					$k8_repo['uri']       	= 'https://bitbucket.org/' . $owner_repo;
					$owner_repo             = explode( '/', $owner_repo );
					$k8_repo['owner']     	= $owner_repo[0];
					$k8_repo['repo']      	= $owner_repo[1];
					$k8_repo['local_path']	= get_theme_root() . '/' . $k8_repo['repo'] .'/';
						
					break;
				case 'Bitbucket Branch':
					if ( empty( $headers['Bitbucket Branch'] ) ) {
						break;
					}
					$k8_repo['branch']      = $headers['Bitbucket Branch'];
					break;
			}
		}

		return $k8_repo;
	}


	/**
	 * Get filename of changelog and return
	 *
	 * @param $type
	 *
	 * @return bool or variable
	 */
	protected function get_changelog_filename( $plugin ) {
		$changelogs = array( 'CHANGES.md', 'CHANGELOG.md' );

		foreach ( $changelogs as $changes ) {
			if ( file_exists( $plugin->local_path . $changes ) ) {
				return $changes;
			} elseif ( file_exists( $plugin->local_path . strtolower( $changes ) ) ) {
				return strtolower( $changes );
			}
		}

			return false;
	}

	
}

if( is_admin() ) new K8_PluginUpdates();


/**
 * Get remote data from a Bitbucket repo.
 *
 * @package GitHub_Updater_Bitbucket_API
 * @author  Andy Fragen
 */
class K8_Updater_BitBucket_API {
	
	private static $hours;
	
	public $extra_headers;
	
	/**
	 * Constructor.
	 *
	 * @param string $type
	 */
	public function __construct( $type, $headers ) {
			
		self::$hours = 12;
		
		$this->type  = $type;
		$this->extra_headers = $headers;
		
	}

	
	/**
	 * Call the API and return a json decoded body.
	 *
	 * @param string $url
	 *
	 * @return boolean|object
	 */
	protected function api( $url ) {
		$response      = wp_remote_get( $this->get_api_url( $url ) );
		$code          = wp_remote_retrieve_response_code( $response );
		$allowed_codes = array( 200, 404 );

		if ( is_wp_error( $response ) ) {
			return false;
		}
		if ( ! in_array( $code, $allowed_codes, false ) ) {
			return false;
		}

		return json_decode( wp_remote_retrieve_body( $response ) );
	}

	/**
	 * Return API url.
	 *
	 * @param string $endpoint
	 *
	 * @return string
	 */
	protected function get_api_url( $endpoint ) {
		$segments = array(
			'owner' => $this->type->owner,
			'repo'  => $this->type->repo,
		);

		/**
		 * Add or filter the available segments that are used to replace placeholders.
		 *
		 * @param array $segments List of segments.
		 */
		$segments = apply_filters( 'k8_updater_api_segments', $segments );

		foreach ( $segments as $segment => $value ) {
			$endpoint = str_replace( '/:' . $segment, '/' . $value, $endpoint );
		}

		return 'https://bitbucket.org/api/' . $endpoint;
	}

	/**
	 * Read the remote file and parse headers.
	 * Saves headers to transient.
	 *
	 * Uses a transient to limit the calls to the API.
	 */
	public function get_remote_info( $file ) {
		$response = get_transient( $file );

		if ( ! $response ) {
			if ( ! isset( $this->type->branch ) ) {
				$this->type->branch = 'master';
			}
			$response = $this->api( '1.0/repositories/:owner/:repo/src/' . trailingslashit( $this->type->branch ) . $file );

			if ( $response ) {
				$contents = $response->data;
				$response = $this->get_file_headers( $contents, $this->type->type );
				$this->set_transient( $file, $response );
			}
		}

		if ( ! $response || isset( $response->message ) ) {
			return false;
		}

		if ( ! is_array( $response ) ) {
			return false;
		}
		$this->type->transient            = $response;
		$this->type->remote_version       = strtolower( $response['Version'] );
		$this->type->branch               = ( ! empty( $response['Bitbucket Branch'] ) ? $response['Bitbucket Branch'] : 'master' );
		$this->type->requires_wp_version  = ( ! empty( $response['Requires WP'] ) ? $response['Requires WP'] : $this->type->requires_wp_version );
		$this->type->requires_php_version = ( ! empty( $response['Requires PHP'] ) ? $response['Requires PHP'] : $this->type->requires_php_version );

		return true;
	}

	/**
	 * Parse the remote info to find most recent tag if tags exist
	 *
	 * Uses a transient to limit the calls to the API.
	 *
	 * @return string latest tag.
	 */
	public function get_remote_tag() {
		$download_link_base = 'https://bitbucket.org/' . trailingslashit( $this->type->owner ) . $this->type->repo . '/get/';
		$response           = $this->get_transient( 'tags' );

		if ( ! $response ) {
			$response = $this->api( '1.0/repositories/:owner/:repo/tags' );
			$arr_resp = (array) $response;

			if ( ! $response || ! $arr_resp ) {
				$response->message = 'No tags found';
			}

			if ( $response ) {
				$this->set_transient( 'tags', $response );
			}
		}

		if ( ! $response || isset( $response->message ) ) {
			return false;
		}

		// Sort and get newest tag
		$tags     = array();
		$rollback = array();
		if ( false !== $response ) {
			foreach ( (array) $response as $num => $tag ) {
				if ( isset( $num ) ) {
					$tags[]           = $num;
					$rollback[ $num ] = $download_link_base . $num . '.zip';
				}
			}
		}

		// no tags are present, exit early
		if ( empty( $tags ) ) {
			return false;
		}

		usort( $tags, 'version_compare' );
		krsort( $rollback );

		$newest_tag             = null;
		$newest_tag_key         = key( array_slice( $tags, -1, 1, true ) );
		$newest_tag             = $tags[ $newest_tag_key ];

		$this->type->newest_tag = $newest_tag;
		$this->type->tags       = $tags;
		$this->type->rollback   = $rollback;
	}

	/**
	 * Construct $download_link
	 *
	 * @param boolean $rollback for theme rollback
	 * 
	 * @return URI
	 */
	public function construct_download_link( $rollback = false ) {
		$download_link_base = 'https://bitbucket.org/' . trailingslashit( $this->type->owner ) . $this->type->repo . '/get/';
		$endpoint           = '';

		// check for rollback
		if ( ! empty( $_GET['rollback'] ) && 'upgrade-theme' === $_GET['action'] && $_GET['theme'] === $this->type->repo ) {
			$endpoint .= $rollback . '.zip';
		
		// for users wanting to update against branch other than master or not using tags, else use newest_tag
		} else if ( ( 'master' != $this->type->branch && ( -1 != version_compare( $this->type->remote_version, $this->type->local_version ) ) || ( '0.0.0' === $this->type->newest_tag ) ) ) {
			$endpoint .= $this->type->branch . '.zip';
		} else {
			$endpoint .= $this->type->newest_tag . '.zip';
		}

		return $download_link_base . $endpoint;
	}

	/**
	 * Read the remote CHANGES.md file
	 *
	 * Uses a transient to limit calls to the API.
	 *
	 * @param $changes
	 *
	 * @return bool
	 */
	public function get_remote_changes( $changes ) {
		$response = $this->get_transient( 'changes' );

		if ( ! $response ) {
			if ( ! isset( $this->type->branch ) ) {
				$this->type->branch = 'master';
			}
			$response = $this->api( '1.0/repositories/:owner/:repo/src/' . trailingslashit( $this->type->branch ) . $changes );

			if ( ! $response ) {
				$response['message'] = 'No changelog found';
				$response = (object) $response;
			}

			if ( $response ) {
				$this->set_transient( 'changes', $response );
			}
		}

		if ( ! $response || isset( $response->message ) ) {
			return false;
		}

		$changelog = $this->get_transient( 'changelog' );

		if ( ! $changelog ) {
			$parser    = new Parsedown();
			$changelog = $parser->text( $response->data );
			$this->set_transient( 'changelog', $changelog );
		}

		$this->type->sections['changelog'] = $changelog;
	}
	
	/**
	 * Read the repository meta from API
	 *
	 * Uses a transient to limit calls to the API
	 *
	 * @return base64 decoded repository meta data
	 */
	public function get_repo_meta() {
		$response = $this->get_transient( 'meta' );

		if ( ! $response ) {
			$response = $this->api( '2.0/repositories/:owner/:repo' );

			if ( $response ) {
				$this->set_transient( 'meta', $response );
			}
		}

		if ( ! $response || isset( $response->message ) ) {
			return false;
		}

		$this->type->repo_meta = $response;
		$this->add_meta_repo_object();
	}

	/**
	 * Add remote data to type object
	 */
	private function add_meta_repo_object() {
		//$this->type->rating       = $this->make_rating( $this->type->repo_meta );
		$this->type->last_updated = $this->type->repo_meta->updated_on;
		$this->type->num_ratings  = $this->type->watchers;
	}
	
	/**
	 * Take remote file contents as string and parse headers.
	 *
	 * @param $contents
	 * @param $type
	 *
	 * @return array
	 */
	protected function get_file_headers( $contents, $type ) {

		$default_plugin_headers = array(
			'Name'        => 'Plugin Name',
			'PluginURI'   => 'Plugin URI',
			'Version'     => 'Version',
			'Description' => 'Description',
			'Author'      => 'Author',
			'AuthorURI'   => 'Author URI',
			'TextDomain'  => 'Text Domain',
			'DomainPath'  => 'Domain Path',
			'Network'     => 'Network',
		);

		$default_theme_headers = array(
			'Name'        => 'Theme Name',
			'ThemeURI'    => 'Theme URI',
			'Description' => 'Description',
			'Author'      => 'Author',
			'AuthorURI'   => 'Author URI',
			'Version'     => 'Version',
			'Template'    => 'Template',
			'Status'      => 'Status',
			'Tags'        => 'Tags',
			'TextDomain'  => 'Text Domain',
			'DomainPath'  => 'Domain Path',
		);

		if ( false !== strpos( $type, 'plugin' ) ) {
			$all_headers = $default_plugin_headers;
		}

		if ( false !== strpos( $type, 'theme' ) ) {
			$all_headers = $default_theme_headers;
		}

		// Make sure we catch CR-only line endings.
		$file_data = str_replace( "\r", "\n", $contents );

		// Merge extra headers and default headers.
		$all_headers = array_merge( $this->extra_headers, (array) $all_headers );
		$all_headers = array_unique( $all_headers );

		foreach ( $all_headers as $field => $regex ) {
			if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( $regex, '/' ) . ':(.*)$/mi', $file_data, $match ) && $match[1] ) {
				$all_headers[ $field ] = _cleanup_header_comment( $match[1] );
			} else {
				$all_headers[ $field ] = '';
			}
		}

		return $all_headers;
	}

	public function get_transient( $key ){
		
		return get_transient( $this->type->slug . '_' . $key );
		
	}
	
	public function set_transient( $key, $value ){
		return set_transient( $this->type->slug . '_' . $key, $value, self::$hours );
	}

}

#
#
# Parsedown
# http://parsedown.org
#
# (c) Emanuil Rusev
# http://erusev.com
#
# For the full license information, view the LICENSE file that was distributed
# with this source code.
#
#

class Parsedown
{
    #
    # Philosophy

    # Parsedown recognises that the Markdown syntax is optimised for humans so
    # it tries to read like one. It goes through text line by line. It looks at
    # how lines start to identify blocks. It looks for special characters to
    # identify inline elements.

    #
    # ~

    function text($text)
    {
        # make sure no definitions are set
        $this->Definitions = array();

        # standardize line breaks
        $text = str_replace("\r\n", "\n", $text);
        $text = str_replace("\r", "\n", $text);

        # replace tabs with spaces
        $text = str_replace("\t", '    ', $text);

        # remove surrounding line breaks
        $text = trim($text, "\n");

        # split text into lines
        $lines = explode("\n", $text);

        # iterate through lines to identify blocks
        $markup = $this->lines($lines);

        # trim line breaks
        $markup = trim($markup, "\n");

        return $markup;
    }

    #
    # Setters
    #

    private $breaksEnabled;

    function setBreaksEnabled($breaksEnabled)
    {
        $this->breaksEnabled = $breaksEnabled;

        return $this;
    }

    #
    # Lines
    #

    protected $BlockTypes = array(
        '#' => array('Atx'),
        '*' => array('Rule', 'List'),
        '+' => array('List'),
        '-' => array('Setext', 'Table', 'Rule', 'List'),
        '0' => array('List'),
        '1' => array('List'),
        '2' => array('List'),
        '3' => array('List'),
        '4' => array('List'),
        '5' => array('List'),
        '6' => array('List'),
        '7' => array('List'),
        '8' => array('List'),
        '9' => array('List'),
        ':' => array('Table'),
        '<' => array('Comment', 'Markup'),
        '=' => array('Setext'),
        '>' => array('Quote'),
        '_' => array('Rule'),
        '`' => array('FencedCode'),
        '|' => array('Table'),
        '~' => array('FencedCode'),
    );

    # ~

    protected $DefinitionTypes = array(
        '[' => array('Reference'),
    );

    # ~

    protected $unmarkedBlockTypes = array(
        'CodeBlock',
    );

    #
    # Blocks
    #

    private function lines(array $lines)
    {
        $CurrentBlock = null;

        foreach ($lines as $line)
        {
            if (chop($line) === '')
            {
                if (isset($CurrentBlock))
                {
                    $CurrentBlock['interrupted'] = true;
                }

                continue;
            }

            $indent = 0;

            while (isset($line[$indent]) and $line[$indent] === ' ')
            {
                $indent ++;
            }

            $text = $indent > 0 ? substr($line, $indent) : $line;

            # ~

            $Line = array('body' => $line, 'indent' => $indent, 'text' => $text);

            # ~

            if (isset($CurrentBlock['incomplete']))
            {
                $Block = $this->{'addTo'.$CurrentBlock['type']}($Line, $CurrentBlock);

                if (isset($Block))
                {
                    $CurrentBlock = $Block;

                    continue;
                }
                else
                {
                    if (method_exists($this, 'complete'.$CurrentBlock['type']))
                    {
                        $CurrentBlock = $this->{'complete'.$CurrentBlock['type']}($CurrentBlock);
                    }

                    unset($CurrentBlock['incomplete']);
                }
            }

            # ~

            $marker = $text[0];

            if (isset($this->DefinitionTypes[$marker]))
            {
                foreach ($this->DefinitionTypes[$marker] as $definitionType)
                {
                    $Definition = $this->{'identify'.$definitionType}($Line, $CurrentBlock);

                    if (isset($Definition))
                    {
                        $this->Definitions[$definitionType][$Definition['id']] = $Definition['data'];

                        continue 2;
                    }
                }
            }

            # ~

            $blockTypes = $this->unmarkedBlockTypes;

            if (isset($this->BlockTypes[$marker]))
            {
                foreach ($this->BlockTypes[$marker] as $blockType)
                {
                    $blockTypes []= $blockType;
                }
            }

            #
            # ~

            foreach ($blockTypes as $blockType)
            {
                $Block = $this->{'identify'.$blockType}($Line, $CurrentBlock);

                if (isset($Block))
                {
                    $Block['type'] = $blockType;

                    if ( ! isset($Block['identified']))
                    {
                        $Elements []= $CurrentBlock['element'];

                        $Block['identified'] = true;
                    }

                    if (method_exists($this, 'addTo'.$blockType))
                    {
                        $Block['incomplete'] = true;
                    }

                    $CurrentBlock = $Block;

                    continue 2;
                }
            }

            # ~

            if (isset($CurrentBlock) and ! isset($CurrentBlock['type']) and ! isset($CurrentBlock['interrupted']))
            {
                $CurrentBlock['element']['text'] .= "\n".$text;
            }
            else
            {
                $Elements []= $CurrentBlock['element'];

                $CurrentBlock = $this->buildParagraph($Line);

                $CurrentBlock['identified'] = true;
            }
        }

        # ~

        if (isset($CurrentBlock['incomplete']) and method_exists($this, 'complete'.$CurrentBlock['type']))
        {
            $CurrentBlock = $this->{'complete'.$CurrentBlock['type']}($CurrentBlock);
        }

        # ~

        $Elements []= $CurrentBlock['element'];

        unset($Elements[0]);

        # ~

        $markup = $this->elements($Elements);

        # ~

        return $markup;
    }

    #
    # Atx

    protected function identifyAtx($Line)
    {
        if (isset($Line['text'][1]))
        {
            $level = 1;

            while (isset($Line['text'][$level]) and $Line['text'][$level] === '#')
            {
                $level ++;
            }

            $text = trim($Line['text'], '# ');

            $Block = array(
                'element' => array(
                    'name' => 'h' . min(6, $level),
                    'text' => $text,
                    'handler' => 'line',
                ),
            );

            return $Block;
        }
    }

    #
    # Code

    protected function identifyCodeBlock($Line)
    {
        if ($Line['indent'] >= 4)
        {
            $text = substr($Line['body'], 4);

            $Block = array(
                'element' => array(
                    'name' => 'pre',
                    'handler' => 'element',
                    'text' => array(
                        'name' => 'code',
                        'text' => $text,
                    ),
                ),
            );

            return $Block;
        }
    }

    protected function addToCodeBlock($Line, $Block)
    {
        if ($Line['indent'] >= 4)
        {
            if (isset($Block['interrupted']))
            {
                $Block['element']['text']['text'] .= "\n";

                unset($Block['interrupted']);
            }

            $Block['element']['text']['text'] .= "\n";

            $text = substr($Line['body'], 4);

            $Block['element']['text']['text'] .= $text;

            return $Block;
        }
    }

    protected function completeCodeBlock($Block)
    {
        $text = $Block['element']['text']['text'];

        $text = htmlspecialchars($text, ENT_NOQUOTES, 'UTF-8');

        $Block['element']['text']['text'] = $text;

        return $Block;
    }

    #
    # Comment

    protected function identifyComment($Line)
    {
        if (isset($Line['text'][3]) and $Line['text'][3] === '-' and $Line['text'][2] === '-' and $Line['text'][1] === '!')
        {
            $Block = array(
                'element' => $Line['body'],
            );

            if (preg_match('/-->$/', $Line['text']))
            {
                $Block['closed'] = true;
            }

            return $Block;
        }
    }

    protected function addToComment($Line, array $Block)
    {
        if (isset($Block['closed']))
        {
            return;
        }

        $Block['element'] .= "\n" . $Line['body'];

        if (preg_match('/-->$/', $Line['text']))
        {
            $Block['closed'] = true;
        }

        return $Block;
    }

    #
    # Fenced Code

    protected function identifyFencedCode($Line)
    {
        if (preg_match('/^(['.$Line['text'][0].']{3,})[ ]*([\w-]+)?[ ]*$/', $Line['text'], $matches))
        {
            $Element = array(
                'name' => 'code',
                'text' => '',
            );

            if (isset($matches[2]))
            {
                $class = 'language-'.$matches[2];

                $Element['attributes'] = array(
                    'class' => $class,
                );
            }

            $Block = array(
                'char' => $Line['text'][0],
                'element' => array(
                    'name' => 'pre',
                    'handler' => 'element',
                    'text' => $Element,
                ),
            );

            return $Block;
        }
    }

    protected function addToFencedCode($Line, $Block)
    {
        if (isset($Block['complete']))
        {
            return;
        }

        if (isset($Block['interrupted']))
        {
            $Block['element']['text']['text'] .= "\n";

            unset($Block['interrupted']);
        }

        if (preg_match('/^'.$Block['char'].'{3,}[ ]*$/', $Line['text']))
        {
            $Block['element']['text']['text'] = substr($Block['element']['text']['text'], 1);

            $Block['complete'] = true;

            return $Block;
        }

        $Block['element']['text']['text'] .= "\n".$Line['body'];;

        return $Block;
    }

    protected function completeFencedCode($Block)
    {
        $text = $Block['element']['text']['text'];

        $text = htmlspecialchars($text, ENT_NOQUOTES, 'UTF-8');

        $Block['element']['text']['text'] = $text;

        return $Block;
    }

    #
    # List

    protected function identifyList($Line)
    {
        list($name, $pattern) = $Line['text'][0] <= '-' ? array('ul', '[*+-]') : array('ol', '[0-9]+[.]');

        if (preg_match('/^('.$pattern.'[ ]+)(.*)/', $Line['text'], $matches))
        {
            $Block = array(
                'indent' => $Line['indent'],
                'pattern' => $pattern,
                'element' => array(
                    'name' => $name,
                    'handler' => 'elements',
                ),
            );

            $Block['li'] = array(
                'name' => 'li',
                'handler' => 'li',
                'text' => array(
                    $matches[2],
                ),
            );

            $Block['element']['text'] []= & $Block['li'];

            return $Block;
        }
    }

    protected function addToList($Line, array $Block)
    {
        if ($Block['indent'] === $Line['indent'] and preg_match('/^'.$Block['pattern'].'[ ]+(.*)/', $Line['text'], $matches))
        {
            if (isset($Block['interrupted']))
            {
                $Block['li']['text'] []= '';

                unset($Block['interrupted']);
            }

            unset($Block['li']);

            $Block['li'] = array(
                'name' => 'li',
                'handler' => 'li',
                'text' => array(
                    $matches[1],
                ),
            );

            $Block['element']['text'] []= & $Block['li'];

            return $Block;
        }

        if ( ! isset($Block['interrupted']))
        {
            $text = preg_replace('/^[ ]{0,4}/', '', $Line['body']);

            $Block['li']['text'] []= $text;

            return $Block;
        }

        if ($Line['indent'] > 0)
        {
            $Block['li']['text'] []= '';

            $text = preg_replace('/^[ ]{0,4}/', '', $Line['body']);

            $Block['li']['text'] []= $text;

            unset($Block['interrupted']);

            return $Block;
        }
    }

    #
    # Quote

    protected function identifyQuote($Line)
    {
        if (preg_match('/^>[ ]?(.*)/', $Line['text'], $matches))
        {
            $Block = array(
                'element' => array(
                    'name' => 'blockquote',
                    'handler' => 'lines',
                    'text' => (array) $matches[1],
                ),
            );

            return $Block;
        }
    }

    protected function addToQuote($Line, array $Block)
    {
        if ($Line['text'][0] === '>' and preg_match('/^>[ ]?(.*)/', $Line['text'], $matches))
        {
            if (isset($Block['interrupted']))
            {
                $Block['element']['text'] []= '';

                unset($Block['interrupted']);
            }

            $Block['element']['text'] []= $matches[1];

            return $Block;
        }

        if ( ! isset($Block['interrupted']))
        {
            $Block['element']['text'] []= $Line['text'];

            return $Block;
        }
    }

    #
    # Rule

    protected function identifyRule($Line)
    {
        if (preg_match('/^(['.$Line['text'][0].'])([ ]{0,2}\1){2,}[ ]*$/', $Line['text']))
        {
            $Block = array(
                'element' => array(
                    'name' => 'hr'
                ),
            );

            return $Block;
        }
    }

    #
    # Setext

    protected function identifySetext($Line, array $Block = null)
    {
        if ( ! isset($Block) or isset($Block['type']) or isset($Block['interrupted']))
        {
            return;
        }

        if (chop($Line['text'], $Line['text'][0]) === '')
        {
            $Block['element']['name'] = $Line['text'][0] === '=' ? 'h1' : 'h2';

            return $Block;
        }
    }

    #
    # Markup

    protected function identifyMarkup($Line)
    {
        if (preg_match('/^<(\w[\w\d]*)(?:[ ][^>]*)?(\/?)[ ]*>/', $Line['text'], $matches))
        {
            if (in_array($matches[1], $this->textLevelElements))
            {
                return;
            }

            $Block = array(
                'element' => $Line['body'],
            );

            if ($matches[2] or $matches[1] === 'hr' or preg_match('/<\/'.$matches[1].'>[ ]*$/', $Line['text']))
            {
                $Block['closed'] = true;
            }
            else
            {
                $Block['depth'] = 0;
                $Block['name'] = $matches[1];
            }

            return $Block;
        }
    }

    protected function addToMarkup($Line, array $Block)
    {
        if (isset($Block['closed']))
        {
            return;
        }

        if (preg_match('/<'.$Block['name'].'([ ][^\/]+)?>/', $Line['text'])) # opening tag
        {
            $Block['depth'] ++;
        }

        if (stripos($Line['text'], '</'.$Block['name'].'>') !== false) # closing tag
        {
            if ($Block['depth'] > 0)
            {
                $Block['depth'] --;
            }
            else
            {
                $Block['closed'] = true;
            }
        }

        $Block['element'] .= "\n".$Line['body'];

        return $Block;
    }

    #
    # Table

    protected function identifyTable($Line, array $Block = null)
    {
        if ( ! isset($Block) or isset($Block['type']) or isset($Block['interrupted']))
        {
            return;
        }

        if (strpos($Block['element']['text'], '|') !== false and chop($Line['text'], ' -:|') === '')
        {
            $alignments = array();

            $divider = $Line['text'];

            $divider = trim($divider);
            $divider = trim($divider, '|');

            $dividerCells = explode('|', $divider);

            foreach ($dividerCells as $dividerCell)
            {
                $dividerCell = trim($dividerCell);

                if ($dividerCell === '')
                {
                    continue;
                }

                $alignment = null;

                if ($dividerCell[0] === ':')
                {
                    $alignment = 'left';
                }

                if (substr($dividerCell, -1) === ':')
                {
                    $alignment = $alignment === 'left' ? 'center' : 'right';
                }

                $alignments []= $alignment;
            }

            # ~

            $HeaderElements = array();

            $header = $Block['element']['text'];

            $header = trim($header);
            $header = trim($header, '|');

            $headerCells = explode('|', $header);

            foreach ($headerCells as $index => $headerCell)
            {
                $headerCell = trim($headerCell);

                $HeaderElement = array(
                    'name' => 'th',
                    'text' => $headerCell,
                    'handler' => 'line',
                );

                if (isset($alignments[$index]))
                {
                    $alignment = $alignments[$index];

                    $HeaderElement['attributes'] = array(
                        'align' => $alignment,
                    );
                }

                $HeaderElements []= $HeaderElement;
            }

            # ~

            $Block = array(
                'alignments' => $alignments,
                'identified' => true,
                'element' => array(
                    'name' => 'table',
                    'handler' => 'elements',
                ),
            );

            $Block['element']['text'] []= array(
                'name' => 'thead',
                'handler' => 'elements',
            );

            $Block['element']['text'] []= array(
                'name' => 'tbody',
                'handler' => 'elements',
                'text' => array(),
            );

            $Block['element']['text'][0]['text'] []= array(
                'name' => 'tr',
                'handler' => 'elements',
                'text' => $HeaderElements,
            );

            return $Block;
        }
    }

    protected function addToTable($Line, array $Block)
    {
        if ($Line['text'][0] === '|' or strpos($Line['text'], '|'))
        {
            $Elements = array();

            $row = $Line['text'];

            $row = trim($row);
            $row = trim($row, '|');

            $cells = explode('|', $row);

            foreach ($cells as $index => $cell)
            {
                $cell = trim($cell);

                $Element = array(
                    'name' => 'td',
                    'handler' => 'line',
                    'text' => $cell,
                );

                if (isset($Block['alignments'][$index]))
                {
                    $Element['attributes'] = array(
                        'align' => $Block['alignments'][$index],
                    );
                }

                $Elements []= $Element;
            }

            $Element = array(
                'name' => 'tr',
                'handler' => 'elements',
                'text' => $Elements,
            );

            $Block['element']['text'][1]['text'] []= $Element;

            return $Block;
        }
    }

    #
    # Definitions
    #

    protected function identifyReference($Line)
    {
        if (preg_match('/^\[(.+?)\]:[ ]*<?(\S+?)>?(?:[ ]+["\'(](.+)["\')])?[ ]*$/', $Line['text'], $matches))
        {
            $Definition = array(
                'id' => strtolower($matches[1]),
                'data' => array(
                    'url' => $matches[2],
                ),
            );

            if (isset($matches[3]))
            {
                $Definition['data']['title'] = $matches[3];
            }

            return $Definition;
        }
    }

    #
    # ~
    #

    protected function buildParagraph($Line)
    {
        $Block = array(
            'element' => array(
                'name' => 'p',
                'text' => $Line['text'],
                'handler' => 'line',
            ),
        );

        return $Block;
    }

    #
    # ~
    #

    protected function element(array $Element)
    {
        $markup = '<'.$Element['name'];

        if (isset($Element['attributes']))
        {
            foreach ($Element['attributes'] as $name => $value)
            {
                $markup .= ' '.$name.'="'.$value.'"';
            }
        }

        if (isset($Element['text']))
        {
            $markup .= '>';

            if (isset($Element['handler']))
            {
                $markup .= $this->$Element['handler']($Element['text']);
            }
            else
            {
                $markup .= $Element['text'];
            }

            $markup .= '</'.$Element['name'].'>';
        }
        else
        {
            $markup .= ' />';
        }

        return $markup;
    }

    protected function elements(array $Elements)
    {
        $markup = '';

        foreach ($Elements as $Element)
        {
            if ($Element === null)
            {
                continue;
            }

            $markup .= "\n";

            if (is_string($Element)) # because of Markup
            {
                $markup .= $Element;

                continue;
            }

            $markup .= $this->element($Element);
        }

        $markup .= "\n";

        return $markup;
    }

    #
    # Spans
    #

    protected $SpanTypes = array(
        '!' => array('Link'), # ?
        '&' => array('Ampersand'),
        '*' => array('Emphasis'),
        '/' => array('Url'),
        '<' => array('UrlTag', 'EmailTag', 'Tag', 'LessThan'),
        '[' => array('Link'),
        '_' => array('Emphasis'),
        '`' => array('InlineCode'),
        '~' => array('Strikethrough'),
        '\\' => array('EscapeSequence'),
    );

    # ~

    protected $spanMarkerList = '*_!&[</`~\\';

    #
    # ~
    #

    public function line($text)
    {
        $markup = '';

        $remainder = $text;

        $markerPosition = 0;

        while ($excerpt = strpbrk($remainder, $this->spanMarkerList))
        {
            $marker = $excerpt[0];

            $markerPosition += strpos($remainder, $marker);

            $Excerpt = array('text' => $excerpt, 'context' => $text);

            foreach ($this->SpanTypes[$marker] as $spanType)
            {
                $handler = 'identify'.$spanType;

                $Span = $this->$handler($Excerpt);

                if ( ! isset($Span))
                {
                    continue;
                }

                # The identified span can be ahead of the marker.

                if (isset($Span['position']) and $Span['position'] > $markerPosition)
                {
                    continue;
                }

                # Spans that start at the position of their marker don't have to set a position.

                if ( ! isset($Span['position']))
                {
                    $Span['position'] = $markerPosition;
                }

                $plainText = substr($text, 0, $Span['position']);

                $markup .= $this->readPlainText($plainText);

                $markup .= isset($Span['markup']) ? $Span['markup'] : $this->element($Span['element']);

                $text = substr($text, $Span['position'] + $Span['extent']);

                $remainder = $text;

                $markerPosition = 0;

                continue 2;
            }

            $remainder = substr($excerpt, 1);

            $markerPosition ++;
        }

        $markup .= $this->readPlainText($text);

        return $markup;
    }

    #
    # ~
    #

    protected function identifyUrl($Excerpt)
    {
        if ( ! isset($Excerpt['text'][1]) or $Excerpt['text'][1] !== '/')
        {
            return;
        }

        if (preg_match('/\bhttps?:[\/]{2}[^\s<]+\b\/*/ui', $Excerpt['context'], $matches, PREG_OFFSET_CAPTURE))
        {
            $url = str_replace(array('&', '<'), array('&amp;', '&lt;'), $matches[0][0]);

            return array(
                'extent' => strlen($matches[0][0]),
                'position' => $matches[0][1],
                'element' => array(
                    'name' => 'a',
                    'text' => $url,
                    'attributes' => array(
                        'href' => $url,
                    ),
                ),
            );
        }
    }

    protected function identifyAmpersand($Excerpt)
    {
        if ( ! preg_match('/^&#?\w+;/', $Excerpt['text']))
        {
            return array(
                'markup' => '&amp;',
                'extent' => 1,
            );
        }
    }

    protected function identifyStrikethrough($Excerpt)
    {
        if ( ! isset($Excerpt['text'][1]))
        {
            return;
        }

        if ($Excerpt['text'][1] === '~' and preg_match('/^~~(?=\S)(.+?)(?<=\S)~~/', $Excerpt['text'], $matches))
        {
            return array(
                'extent' => strlen($matches[0]),
                'element' => array(
                    'name' => 'del',
                    'text' => $matches[1],
                    'handler' => 'line',
                ),
            );
        }
    }

    protected function identifyEscapeSequence($Excerpt)
    {
        if (isset($Excerpt['text'][1]) and in_array($Excerpt['text'][1], $this->specialCharacters))
        {
            return array(
                'markup' => $Excerpt['text'][1],
                'extent' => 2,
            );
        }
    }

    protected function identifyLessThan()
    {
        return array(
            'markup' => '&lt;',
            'extent' => 1,
        );
    }

    protected function identifyUrlTag($Excerpt)
    {
        if (strpos($Excerpt['text'], '>') !== false and preg_match('/^<(https?:[\/]{2}[^\s]+?)>/i', $Excerpt['text'], $matches))
        {
            $url = str_replace(array('&', '<'), array('&amp;', '&lt;'), $matches[1]);

            return array(
                'extent' => strlen($matches[0]),
                'element' => array(
                    'name' => 'a',
                    'text' => $url,
                    'attributes' => array(
                        'href' => $url,
                    ),
                ),
            );
        }
    }

    protected function identifyEmailTag($Excerpt)
    {
        if (strpos($Excerpt['text'], '>') !== false and preg_match('/^<(\S+?@\S+?)>/', $Excerpt['text'], $matches))
        {
            return array(
                'extent' => strlen($matches[0]),
                'element' => array(
                    'name' => 'a',
                    'text' => $matches[1],
                    'attributes' => array(
                        'href' => 'mailto:'.$matches[1],
                    ),
                ),
            );
        }
    }

    protected function identifyTag($Excerpt)
    {
        if (strpos($Excerpt['text'], '>') !== false and preg_match('/^<\/?\w.*?>/', $Excerpt['text'], $matches))
        {
            return array(
                'markup' => $matches[0],
                'extent' => strlen($matches[0]),
            );
        }
    }

    protected function identifyInlineCode($Excerpt)
    {
        $marker = $Excerpt['text'][0];

        if (preg_match('/^('.$marker.'+)[ ]*(.+?)[ ]*(?<!'.$marker.')\1(?!'.$marker.')/', $Excerpt['text'], $matches))
        {
            $text = $matches[2];
            $text = htmlspecialchars($text, ENT_NOQUOTES, 'UTF-8');

            return array(
                'extent' => strlen($matches[0]),
                'element' => array(
                    'name' => 'code',
                    'text' => $text,
                ),
            );
        }
    }

    protected function identifyLink($Excerpt)
    {
        $extent = $Excerpt['text'][0] === '!' ? 1 : 0;

        if (strpos($Excerpt['text'], ']') and preg_match('/\[((?:[^][]|(?R))*)\]/', $Excerpt['text'], $matches))
        {
            $Link = array('text' => $matches[1], 'label' => strtolower($matches[1]));

            $extent += strlen($matches[0]);

            $substring = substr($Excerpt['text'], $extent);

            if (preg_match('/^\s*\[([^][]+)\]/', $substring, $matches))
            {
                $Link['label'] = strtolower($matches[1]);

                if (isset($this->Definitions['Reference'][$Link['label']]))
                {
                    $Link += $this->Definitions['Reference'][$Link['label']];

                    $extent += strlen($matches[0]);
                }
                else
                {
                    return;
                }
            }
            elseif (isset($this->Definitions['Reference'][$Link['label']]))
            {
                $Link += $this->Definitions['Reference'][$Link['label']];

                if (preg_match('/^[ ]*\[\]/', $substring, $matches))
                {
                    $extent += strlen($matches[0]);
                }
            }
            elseif (preg_match('/^\([ ]*(.*?)(?:[ ]+[\'"](.+?)[\'"])?[ ]*\)/', $substring, $matches))
            {
                $Link['url'] = $matches[1];

                if (isset($matches[2]))
                {
                    $Link['title'] = $matches[2];
                }

                $extent += strlen($matches[0]);
            }
            else
            {
                return;
            }
        }
        else
        {
            return;
        }

        $url = str_replace(array('&', '<'), array('&amp;', '&lt;'), $Link['url']);

        if ($Excerpt['text'][0] === '!')
        {
            $Element = array(
                'name' => 'img',
                'attributes' => array(
                    'alt' => $Link['text'],
                    'src' => $url,
                ),
            );
        }
        else
        {
            $Element = array(
                'name' => 'a',
                'handler' => 'line',
                'text' => $Link['text'],
                'attributes' => array(
                    'href' => $url,
                ),
            );
        }

        if (isset($Link['title']))
        {
            $Element['attributes']['title'] = $Link['title'];
        }

        return array(
            'extent' => $extent,
            'element' => $Element,
        );
    }

    protected function identifyEmphasis($Excerpt)
    {
        if ( ! isset($Excerpt['text'][1]))
        {
            return;
        }

        $marker = $Excerpt['text'][0];

        if ($Excerpt['text'][1] === $marker and preg_match($this->StrongRegex[$marker], $Excerpt['text'], $matches))
        {
            $emphasis = 'strong';
        }
        elseif (preg_match($this->EmRegex[$marker], $Excerpt['text'], $matches))
        {
            $emphasis = 'em';
        }
        else
        {
            return;
        }

        return array(
            'extent' => strlen($matches[0]),
            'element' => array(
                'name' => $emphasis,
                'handler' => 'line',
                'text' => $matches[1],
            ),
        );
    }

    #
    # ~

    protected function readPlainText($text)
    {
        $breakMarker = $this->breaksEnabled ? "\n" : "  \n";

        $text = str_replace($breakMarker, "<br />\n", $text);

        return $text;
    }

    #
    # ~
    #

    protected function li($lines)
    {
        $markup = $this->lines($lines);

        $trimmedMarkup = trim($markup);

        if ( ! in_array('', $lines) and substr($trimmedMarkup, 0, 3) === '<p>')
        {
            $markup = $trimmedMarkup;
            $markup = substr($markup, 3);

            $position = strpos($markup, "</p>");

            $markup = substr_replace($markup, '', $position, 4);
        }

        return $markup;
    }

    #
    # Multiton
    #

    static function instance($name = 'default')
    {
        if (isset(self::$instances[$name]))
        {
            return self::$instances[$name];
        }

        $instance = new self();

        self::$instances[$name] = $instance;

        return $instance;
    }

    private static $instances = array();

    #
    # Deprecated Methods
    #

    /**
     * @deprecated in favor of "text"
     */
    function parse($text)
    {
        $markup = $this->text($text);

        return $markup;
    }

    #
    # Fields
    #

    protected $Definitions;

    #
    # Read-only

    protected $specialCharacters = array(
        '\\', '`', '*', '_', '{', '}', '[', ']', '(', ')', '>', '#', '+', '-', '.', '!',
    );

    protected $StrongRegex = array(
        '*' => '/^[*]{2}((?:[^*]|[*][^*]*[*])+?)[*]{2}(?![*])/s',
        '_' => '/^__((?:[^_]|_[^_]*_)+?)__(?!_)/us',
    );

    protected $EmRegex = array(
        '*' => '/^[*]((?:[^*]|[*][*][^*]+?[*][*])+?)[*](?![*])/s',
        '_' => '/^_((?:[^_]|__[^_]*__)+?)_(?!_)\b/us',
    );

    protected $textLevelElements = array(
        'a', 'br', 'bdo', 'abbr', 'blink', 'nextid', 'acronym', 'basefont',
        'b', 'em', 'big', 'cite', 'small', 'spacer', 'listing',
        'i', 'rp', 'del', 'code',          'strike', 'marquee',
        'q', 'rt', 'ins', 'font',          'strong',
        's', 'tt', 'sub', 'mark',
        'u', 'xm', 'sup', 'nobr',
                   'var', 'ruby',
                   'wbr', 'span',
                          'time',
    );
}


?>